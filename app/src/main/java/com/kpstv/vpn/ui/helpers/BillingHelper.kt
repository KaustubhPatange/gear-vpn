package com.kpstv.vpn.ui.helpers

import androidx.activity.ComponentActivity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class BillingHelper(private val activity: ComponentActivity) {

  val isPurchased: Flow<Boolean> = flow { emit(false) }

  fun init() { }

}