package com.kpstv.vpn.ui.components

import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.annotation.RawRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.airbnb.lottie.compose.*
import com.google.accompanist.insets.navigationBarsPadding
import com.kpstv.vpn.R
import com.kpstv.vpn.extensions.utils.AppUtils
import com.kpstv.vpn.extensions.utils.AppUtils.launchUrl
import com.kpstv.vpn.ui.theme.CommonPreviewTheme

@Composable
fun PremiumBottomSheet(
  premiumBottomSheet: MutableState<BottomSheetState>,
  suppressBackPress: (Boolean) -> Unit
) {
  val context = LocalContext.current

  BottomSheet(bottomSheetState = premiumBottomSheet) {

    CommonSheet(
      lottieRes = R.raw.premium_gold,
      text = stringResource(R.string.premium_text),
      onCancel = { premiumBottomSheet.value = BottomSheetState.Collapsed },
      onButtonClick = { context.launchUrl(context.getString(R.string.google_play_page)) },
    )

    val backpress = remember {
      object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
          premiumBottomSheet.value = BottomSheetState.Collapsed
        }
      }
    }

    LaunchedEffect(premiumBottomSheet.value) {
      suppressBackPress.invoke(premiumBottomSheet.value == BottomSheetState.Expanded)
      backpress.isEnabled = premiumBottomSheet.value == BottomSheetState.Expanded
    }

    val dispatcher = checkNotNull(LocalOnBackPressedDispatcherOwner.current).onBackPressedDispatcher
    DisposableEffect(Unit) {
      dispatcher.addCallback(backpress)
      onDispose {
        backpress.remove()
      }
    }
  }
}

@Composable
private fun CommonSheet(
  @RawRes lottieRes: Int,
  text: String,
  onCancel: () -> Unit,
  onButtonClick: () -> Unit
) {
  Box(
    modifier = Modifier
      .padding(horizontal = 5.dp)
      .fillMaxWidth()
      .wrapContentHeight()
      .clip(RoundedCornerShape(topStart = 10.dp, topEnd = 10.dp))
      .background(MaterialTheme.colors.background)
      .padding(horizontal = 15.dp, vertical = 10.dp)
  ) {
    Column {
      Spacer(modifier = Modifier.height(10.dp))
      Text(
        modifier = Modifier,
        text = text,
        style = MaterialTheme.typography.h4.copy(fontSize = 18.sp)
      )
      Spacer(modifier = Modifier.height(20.dp))
      Row {
        ThemeButton(
          modifier = Modifier
            .weight(0.5f)
            .height(50.dp),
          onClick = onCancel,
          text = "CLOSE"
        )
        Spacer(modifier = Modifier.weight(0.5f))
        ThemeButton(
          modifier = Modifier
            .weight(1f)
            .height(50.dp),
          onClick = onButtonClick,
          text = "INSTALL"
        )
      }
      Spacer(modifier = Modifier.navigationBarsPadding())
    }
  }
}

@Preview(showBackground = true)
@Composable
fun PreviewPremiumBottomSheet() {
  CommonPreviewTheme {
    CommonSheet(
      lottieRes = R.raw.premium_gold,
      text = stringResource(R.string.premium_text),
      onCancel = { },
      onButtonClick = { },
    )
  }
}