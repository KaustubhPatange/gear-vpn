package com.kpstv.vpn.extensions.utils

import com.kpstv.vpn.R

object FlagUtils {

  /**
   * Returns the url of flag image for the corresponding name of country/language.
   */
  fun getOrNull(name: String): Int {
    return flagMap.firstNotNullOfOrNull { (key, id) -> if (key.contains(name)) id else null } ?: R.drawable.unknown
  }

  private val flagMap: Map<String, Int> = mapOf(
    "Korea" to R.drawable.korea,
    "United States" to R.drawable.us,
    "United Kingdom" to R.drawable.uk,
    "Japan" to R.drawable.japan,
    "Malaysia" to R.drawable.malaysia,
    "France" to R.drawable.france,
    "Germany" to R.drawable.germany,
    "Spain" to R.drawable.spain,
    "China" to R.drawable.china,
    "Italy" to R.drawable.italy,
    "Brazil" to R.drawable.brazil,
    "Russia" to R.drawable.russia,
    "Russian" to R.drawable.russia,
    "Canada" to R.drawable.canada,
    "Australia" to R.drawable.aus,
    "India" to R.drawable.india,
    "Thailand" to R.drawable.thai,
    "Singapore" to R.drawable.singapore,
    "Vietnam" to R.drawable.vietnam,
    "Viet Nam" to R.drawable.vietnam,
    "Switzerland" to R.drawable.swiz,
    "Sweden" to R.drawable.sweden,
    "Finland" to R.drawable.finland,
    "Mexico" to R.drawable.mexico,
    "Georgia" to R.drawable.georgia,
    "Netherlands" to R.drawable.netherlands,
    "Turkey" to R.drawable.turkey,
    "Belgium" to R.drawable.belgium,
    "Poland" to R.drawable.poland,
    "Portugal" to R.drawable.portugal,
    "Argentina" to R.drawable.argentina,
    "Chile" to R.drawable.chile,
    "Indonesia" to R.drawable.indonesia,
    "Colombia" to R.drawable.colombia,
    "Romania" to R.drawable.romania,
    "Israel" to R.drawable.israel,
    "Taiwan" to R.drawable.taiwan,
    "Hong Kong" to R.drawable.hong_kong,
    "Denmark" to R.drawable.denmark,
    "Philippines" to R.drawable.philippines,
    "Austria" to R.drawable.austria,
    "Peru" to R.drawable.peru,
    "South Africa" to R.drawable.south_africa,
    "Ukraine" to R.drawable.ukraine,
    "Norway" to R.drawable.norway,
    "Czech Republic" to R.drawable.czech_republic,
    "Greece" to R.drawable.greece,
    "Ireland" to R.drawable.ireland,
    "England" to R.drawable.england,
    "Jamaica" to R.drawable.jamaica,
    "New Zealand" to R.drawable.new_zealand,
    "Hungary" to R.drawable.hungary,
    "Egypt" to R.drawable.egypt,
    "Iceland" to R.drawable.iceland,
    "Lithuania" to R.drawable.lithuania,
    "Bangladesh" to R.drawable.bangladesh,
    "Iraq" to R.drawable.iraq,
    "Iran" to R.drawable.iran,
    "Qatar" to R.drawable.qatar,
    "Venezuela" to R.drawable.venezuela,
    "Cambodia" to R.drawable.cambodia,
    "Slovakia" to R.drawable.slovakia,
    "Slovenia" to R.drawable.slovenia,
    )
}